// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAvSSMYYwSxjGtfr9b-0Q9XsSnBF80z-4s",
    authDomain: "userform-483c0.firebaseapp.com",
    databaseURL: "https://userform-483c0.firebaseio.com",
    projectId: "userform-483c0",
    storageBucket: "userform-483c0.appspot.com",
    messagingSenderId: "741963908618",
    appId: "1:741963908618:web:ee5d82bdbbffe0bdcac1f3"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  //Reference messages
  var messagesRef = firebase.database().ref('recipe')

document.getElementById('userform').addEventListener('submit',submitForm)
function submitForm(e){

  
    e.preventDefault();

    
    var uid = getInputVal('uid');
    var name = getInputVal('name');
    var image = document.getElementById("image").files[0];
    var imagename = image.name;
    var instruction = getInputVal('instruction');
    var examount = document.querySelectorAll('#examount');
    var exname = document.querySelectorAll('#exname');
    var items = new Array(examount.length);
    var nutrients;
    for(var i=0;i<examount.length;i++){
        items[i] = examount[i].value + " " + exname[i].value;
        
    }
    var sendData = {
        title:name,
        ingr:items
    }
var dataitems=[]
let tmp;
    for(var i=0;i<examount.length;i++){
        tmp = {amount:examount[i].value,name:exname[i].value}
        dataitems.push(tmp);
        
    }
    console.log(dataitems)
    console.log(JSON.stringify(sendData.ingr))
    url = "https://api.edamam.com/api/nutrition-details?app_id=b4082129&app_key=52130fdd947e1eb779f2946ff184e9d1";
    fetch(url,{
        method:'POST',
        headers:{
            'Content-Type':'application/json',
        },
        body:JSON.stringify(sendData)
    }).then(function(response) {
        response.json().then((data)=>{
            nutrients = data.totalNutrients;
            console.log(nutrients)
            
            saveMessage(uid,name,image,imagename,instruction,nutrients,dataitems);
            
        })
  }).catch(function(error) {
    console.log('Request failed', error)
  });
   
  document.getElementById('result').innerHTML = 'Uploaded!'
}

function getInputVal(id){
    return document.getElementById(id).value
}

//save message to firebase
function saveMessage(uid,name,image,imagename,instruction,nutrients,dataitems){
    
    var storageRef = firebase.storage().ref('images/'+imagename);
    var uploadTask =storageRef.put(image);
    uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL){
        var newMessageRef = messagesRef.push();
        newMessageRef.set({
            uid:uid,
            name:name,
            image:downloadURL,
            recipe: {
                analyzedInstructions: [
                    {
                        steps: instruction
                    }
                ],
                extendedIngredients: dataitems ,
                instructions: instruction,
                nutrition:nutrients
            }
            
        })
      
        
    })
   
   
    
}